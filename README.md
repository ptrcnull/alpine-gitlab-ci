# alpine-gitlab-ci
Build Alpine Linux packages with Gitlab CI

Images are automatically pushed to https://hub.docker.com/r/alpinelinux/alpine-gitlab-ci

[![pipeline status](https://gitlab.alpinelinux.org/alpine/infra/docker/alpine-gitlab-ci/badges/master/pipeline.svg)](https://gitlab.alpinelinux.org/alpine/infra/docker/alpine-gitlab-ci/commits/master)
